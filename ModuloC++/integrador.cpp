#include <stdint.h>
#include <iostream>
#include <vector>

using namespace std;

class Data {
    protected:
        float Temperature;
        float Presion;
        float Humidity;
    public:
        Data(float temp, float pre, float hum);
        Data(); 
        float getTemperature();
        float getPresion();
        float getHumidity();
        void setValues(float temp, float pres, float hum);
        virtual void showData(){
            cout << "Clase base." << endl;
        }
   
};

Data::Data() { 
    Temperature = 0;
    Presion = 0;
    Humidity = 0;
}
Data::Data(float temp, float pre, float hum) { 
    Temperature = temp;
    Presion = pre;
    Humidity = hum;
}

float Data::getTemperature(){
    return Temperature;
}
float Data::getPresion(){
    return Presion;
}
float Data::getHumidity(){
    return Humidity;
}
void Data::setValues(float temp, float pres, float hum){
    Temperature = temp;
    Presion = pres;
    Humidity = hum;
}


class view1: public Data {
    public:
        void showData(){
            cout << Temperature << "C degrees and " << Humidity << "% humidity" << endl;
        }

};

class view2: public Data{
    private:
        float max;
        float min;
        double sum;
        int count;
    public:
        view2();
        void setValues(float temp, float pres, float hum);
        void showData(){
            cout << "Avg/Max/Min Temperature = " << (sum/count) << "/" << max << "/" << min << endl;
        }

};
view2::view2(){
    max = 0;
    min = 999;
    sum = 0;
    count = 0;
}
void view2::setValues(float temp, float pres, float hum){
    if(temp < min){
        min = temp;
    }
    if(temp > max){
        max = temp;
    }
    count++;
    sum += temp;
   // this->setValues(temp, pres, hum);

}
class view3: public Data{
    public:
        void showData(){
            if(Temperature < 5){
                cout << "Forecast: Don't forget to bring your jacket." << endl;
            }
            if(Temperature > 25){
                cout << "Forecast: Do not forget to hydrate if you are going to be outdoors." << endl;
            }
            if((Temperature < 25) && (Temperature > 15)){
                cout << "Forecast: The temperature is ideal." << endl;
            }
        }

};



int main () {
    cout << "****************************************************" << endl;
    cout << "******        Ejercicio Integrador C++        ******" << endl;
    cout << "****************************************************" << endl;
    cout << "******           █ █▄░█ █░█ ▄▀█ █▀█           ******" << endl;
    cout << "******           █ █░▀█ ▀▄▀ █▀█ █▀▀           ******" << endl;
    cout << "****************************************************" << endl;

    float T, H, P, min, max, avg;
    char action = 'y';
    
    vector<Data*> v;
    view1 v1;
    view2 v2;
    view3 v3;
    
	v.push_back(&v1);
	v.push_back(&v2);
	v.push_back(&v3);
	while(1){
        cout << "Ingrese (T[C] H[%] P[KPa]): ";
        cin >> T >> H >> P;
        
        v1.setValues(T, P, H);
        v2.setValues(T, P, H);
        v3.setValues(T, P, H);

		for(int i = 0; i < v.size(); i++) {
			cout << (i+1) << ". ";
            v[i]->showData();
		}

        cout << "\nQuieres agregar nuevos datos? (y/n): ";
        cin >> action;

        if(action == 'n'){
            break;
        }
	}

    cout << "****************************************************" << endl;
    cout << "******           Programa finalizado          ******" << endl;
    cout << "****************************************************" << endl;

    return 0;
}

