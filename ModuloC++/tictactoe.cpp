
#include <stdint.h>
#include <cstring>
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

void showTable(char table[9]) {
    cout << endl; 
    cout << "  " << table[6] <<  " | " << table[7] << " | " << table[8] << endl;
    cout << " -----------" << endl; 
    cout << "  " << table[3] <<  " | " << table[4] << " | " << table[5] << endl;
    cout << " -----------" << endl; 
    cout << "  " << table[0] <<  " | " << table[1] << " | " << table[2] << endl;
    cout << endl; 
}

void showOptionsMode(){
    cout << "Selecciona modo de juego:" << endl;
    cout << "Opcion 1: Jugador contra Jugador." << endl;
    cout << "Opcion 2: Jugador contra máquina." << endl;
}

int checkWin(char table[9]){
    
    if (((table[0] == 'o') && (table[1] == 'o') && (table[2] == 'o')) || 
        ((table[3] == 'o') && (table[4] == 'o') && (table[5] == 'o')) || 
        ((table[6] == 'o') && (table[7] == 'o') && (table[8] == 'o')) || 
        ((table[0] == 'o') && (table[3] == 'o') && (table[6] == 'o')) || 
        ((table[1] == 'o') && (table[4] == 'o') && (table[7] == 'o')) || 
        ((table[2] == 'o') && (table[5] == 'o') && (table[8] == 'o')) || 
        ((table[0] == 'o') && (table[4] == 'o') && (table[8] == 'o')) || 
        ((table[2] == 'o') && (table[4] == 'o') && (table[6] == 'o')))
        {
        return 2;
    }
    if (((table[0] == 'x') && (table[1] == 'x') && (table[2] == 'x')) || 
        ((table[3] == 'x') && (table[4] == 'x') && (table[5] == 'x')) || 
        ((table[6] == 'x') && (table[7] == 'x') && (table[8] == 'x')) || 
        ((table[0] == 'x') && (table[3] == 'x') && (table[6] == 'x')) || 
        ((table[1] == 'x') && (table[4] == 'x') && (table[7] == 'x')) || 
        ((table[2] == 'x') && (table[5] == 'x') && (table[8] == 'x')) || 
        ((table[0] == 'x') && (table[4] == 'x') && (table[8] == 'x')) || 
        ((table[2] == 'x') && (table[4] == 'x') && (table[6] == 'x')))
        {
        return 1;
    }

    return 0;  
}
int getPositionRandom(char table[9]){
    int position = (rand()%9)+1;
    while(table[position-1] != ' '){
        position = (rand()%9)+1;
    }
    return position;
}


string getSimbols(int words){

    string result="";
    for (int i = 0; i < words; i++)
    {
        result.push_back('*');
    }
    return result;
    
}
void modoMultiPlayer(){
    char table[9] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
    string name[2];
    cout << "Ingresar nombre del jugador 1: ";
    cin >> name[0];

    cout << "Ingresar nombre del jugador 2: ";
    cin >> name[1];
    cout << endl;
    
    cout << "*******************************" << getSimbols(name[0].length()) << getSimbols(name[1].length()) <<  endl;
    cout << "******       " << name[0] << " VS " << name[1] << "        ******" << endl;
    cout << "*******************************" << getSimbols(name[0].length()) << getSimbols(name[1].length()) <<  endl;

    cout << "Cada jugador debe elegir el numero de la casilla que desea marcar: " << endl;
    char tableInfo[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    showTable(tableInfo);
   
    cout << "****************************************************" << endl;
    cout << "******              𝕋𝕚𝕔-𝕋𝕒𝕔-𝕋𝕠𝕖               ******" << endl;
    cout << "******           Comienza el juego            ******" << endl;
    cout << "****************************************************" << endl;

    int win = 0, nextPlayer=1, pos, freePositions=9;
            
    while((win == 0) && (freePositions > 0)){
        cout << "Es el turno de " << name[nextPlayer-1] << ": ";
        cin >> pos;

        if((pos < 0) || (pos > 10)){
              
            cout << "***************************************************************" <<  endl;
            cout << "Debes elegir el numero de la posicion que quieres marcar: " << endl;
            showTable(tableInfo);
            cout << "***************************************************************" <<  endl;

            continue;        
        }

        if(table[pos-1] != ' '){

            cout << "******           La posicion ingresada ya esta ocupada           ******" << endl;
            continue;        
        }
        table[pos-1] =  (nextPlayer == 1) ? 'x' : 'o';
        freePositions--;
        win = checkWin(table);
        showTable(table);
        nextPlayer = (nextPlayer == 1) ? 2 : 1;

    }

    if((win == 1) || (win == 2)){
        cout << "**********************************************" << getSimbols(name[win-1].length()) << endl;
        cout << "***** Felicitaciones " << name[win-1] << "!! eres el ganador!!*****" << endl;
        cout << "**********************************************" << getSimbols(name[win-1].length()) << endl;

    }else{
        cout << "***********************************" << endl;
        cout << "***** Han terminado empatados *****" << endl;
        cout << "***********************************" << endl;
    }
}

void modoSinglePlayer(){

    char table[9] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
    string name[1];
    cout << "Ingresar nombre del jugador: ";
    cin >> name[0];
    
    cout << "***************************************************************" <<  endl;

    cout << "Debes elegir el numero de la posicion que quieres marcar: " << endl;
    char tableInfo[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    showTable(tableInfo);
   
    cout << "****************************************************" << endl;
    cout << "******              𝕋𝕚𝕔-𝕋𝕒𝕔-𝕋𝕠𝕖               ******" << endl;
    cout << "******           Comienza el juego            ******" << endl;
    cout << "****************************************************" << endl;
    srand(time(NULL));
    int win = 0, nextPlayer=1, pos, freePositions=9;
            
    while((win == 0) && (freePositions > 0)){
        if(nextPlayer == 1){
            cout << "Es el turno de " << name[0] << ": ";
            cin >> pos;
        }else{
            pos = getPositionRandom(table);
            cout << "La maquina ha elegido la posicion " << pos << endl;
        }
        

        if((pos < 0) || (pos > 10)){
            cout << "***************************************************************" <<  endl;
            cout << "** Debes elegir el numero de la posicion que quieres marcar: **" << endl;
            cout << "***************************************************************" <<  endl;
            showTable(tableInfo);
            cout << "***************************************************************" <<  endl;
            continue;        
        }

        if(table[pos-1] != ' '){
            cout << "******           La posicion ingresada ya esta ocupada           ******" << endl;
            continue;        
        }
        table[pos-1] =  (nextPlayer == 1) ? 'x' : 'o';
        freePositions--;
        win = checkWin(table);
        showTable(table);
        nextPlayer = (nextPlayer == 1) ? 2 : 1;

    }

    switch (win)
    {
    case 1:
        cout << "**********************************************" << getSimbols(name[0].length()) << endl;
        cout << "***** Felicitaciones " << name[win-1] << "!! eres el ganador!!*****" << endl;
        cout << "**********************************************" << getSimbols(name[0].length()) << endl;
        break;
    case 2:
        cout << "**********************************************" << getSimbols(name[0].length()) << endl;
        cout << "***** Lo sentimos " << name[0] << ", has perdido!!  *****" << endl;
        cout << "**********************************************" << getSimbols(name[0].length()) << endl;
        break;
    default:
        cout << "***********************************" << endl;
        cout << "***** Han terminado empatados *****" << endl;
        cout << "***********************************" << endl;
        break;
    }
   
}

int main ()
{   

    int n;

    cout << "****************************************************" << endl;
    cout << "******               𝕋𝕚𝕔-𝕋𝕒𝕔-𝕋𝕠𝕖              ******" << endl;
    cout << "****************************************************" << endl;
    cout << "******           █ █▄░█ █░█ ▄▀█ █▀█           ******" << endl;
    cout << "******           █ █░▀█ ▀▄▀ █▀█ █▀▀           ******" << endl;
    cout << "****************************************************" << endl;

    showOptionsMode();
    cin >> n;

    while((n!= 1) && (n!=2)){
        cout << "***** No se encontro la opcion seleccionada.******" << endl;
        showOptionsMode();
        cin >> n;
    }
    if( n == 1){
        cout << "****************************************************" << endl;
        cout << "******              𝕋𝕚𝕔-𝕋𝕒𝕔-𝕋𝕠𝕖               ******" << endl;
        cout << "****** Modo de juego: Jugador contra Jugador. ******" << endl;
        cout << "****************************************************" << endl;
        modoMultiPlayer();
    }else{
        cout << "****************************************************" << endl;
        cout << "******              𝕋𝕚𝕔-𝕋𝕒𝕔-𝕋𝕠𝕖               ******" << endl;
        cout << "****** Modo de juego: Jugador contra Maquina. ******" << endl;
        cout << "****************************************************" << endl;
        modoSinglePlayer();
    }



    /*
    std::string temp;
    int n,i,j;

    <<



    
    std::cout << "Ingresar el numero de palabras que quieres ordenar  " << std::endl;
    std::cin >> n;
    std::cout << "Debes ingresar " << n << " palabras."<< std::endl;

    std::string retval[n];

    for (i = 0; i < n; i++)
    {
        std::cout << i+1 << ": ";
        std::cin >> retval[i];
    }



    for (i=1; i<n; i++){
        for (j=0 ; j<n - 1; j++){
            if (retval[j] > retval[j+1]){
                temp = retval[j];
                retval[j] = retval[j+1];
                retval[j+1] = temp;
            }
        }
    }

    std::cout << "Las palabras en orden alfabetico son:" << std::endl;
    for (i=0; i<n; i++){
        std::cout << i+1 << "- "<< retval[i] << std::endl;
    }
*/
    return 0;
}


