#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct  {
   char  name[30];
   int   victories;
   int   defeats;
   int   ties;
   int   goals;
}team;

void showTeams(team *teams, int n) {
    printf("************************************************************************\r\n");
    for (int i = 0; i < n; i++)
    {
        printf("* Nombre: %s - Victorias: %i - Derrotas: %i - Empates: %i - Goles: %i \r\n", teams[i].name, teams[i].victories, teams[i].defeats, teams[i].ties, teams[i].goals );
    }
    printf("************************************************************************\r\n");
}
void sortByVictories(team *teams, int n) {
    team *auxTeams;
    team t;
    auxTeams = (team *) malloc(sizeof(team)*n);
    memcpy(auxTeams, teams, sizeof(team)*n);
    for (size_t i = 0; i < n-1; i++) { 
        for (size_t d = 0 ; d < n - i - 1; d++)
        {
            if (auxTeams[d].victories < auxTeams[d+1].victories)
            {
                t          = auxTeams[d];
                auxTeams[d]   = auxTeams[d+1];
                auxTeams[d+1] = t;
            }
        }
    }
    showTeams(auxTeams, n);
    free(auxTeams);
}
void sortByDefeats(team *teams, int n) {
    team *auxTeams;
    team t;
    auxTeams = (team *) malloc(sizeof(team)*n);
    memcpy(auxTeams, teams, sizeof(team)*n);
    for (size_t i = 0; i < n-1; i++) { 
        for (size_t d = 0 ; d < n - i - 1; d++)
        {
            if (auxTeams[d].defeats < auxTeams[d+1].defeats)
            {
                t          = auxTeams[d];
                auxTeams[d]   = auxTeams[d+1];
                auxTeams[d+1] = t;
            }
        }
    }
    showTeams(auxTeams, n);
    free(auxTeams);
}
void sortByGoals(team *teams, int n) {
    team *auxTeams;
    team t;
    auxTeams = (team *) malloc(sizeof(team)*n);
    memcpy(auxTeams, teams, sizeof(team)*n);
    for (size_t i = 0; i < n-1; i++) { 
        for (size_t d = 0 ; d < n - i - 1; d++)
        {
            if (auxTeams[d].goals < auxTeams[d+1].goals)
            {
                t          = auxTeams[d];
                auxTeams[d]   = auxTeams[d+1];
                auxTeams[d+1] = t;
            }
        }
    }
    showTeams(auxTeams, n);
    free(auxTeams);
}

int main()
{
    printf("*****************************************************\r\n");
    printf("******  Punteros y estructuras - Ejercicio 2  *******\r\n");
    printf("*****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           *******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           *******\r\n");
    printf("*****************************************************\r\n");
    
    team *teams;
    teams = (team *) malloc(sizeof(team));

    unsigned int option;
    char moreTeams='y';
    int cantTeams=0;

    while(1){
        printf("Quieres agregar otro equipo? (y/n): ");
        scanf("%s", &moreTeams);
        printf("\r\n");
        if(moreTeams == 'n'){
            if(cantTeams == 0){
                printf("\r\n*********************************************\r\n");
                printf("****   Debes agregar al menos un equipo   ***\r\n");
                printf("*********************************************\r\n");
            }else{
                break;
            } 
        }else{
            if(moreTeams == 'y'){
                cantTeams++;
                teams = (team *) realloc(teams, sizeof(teams)*cantTeams);

                printf("Ingrese el nombre del equipo: ");
                scanf("%s", teams[cantTeams-1].name);
                printf("\r");

                printf("Número de victorias: ");
                scanf("%u", &teams[cantTeams-1].victories);
                printf("\r");

                printf("Número de derrotas: ");
                scanf("%u", &teams[cantTeams-1].defeats);
                printf("\r");

                printf("Número de empates: ");
                scanf("%u", &teams[cantTeams-1].ties);
                printf("\r");

                printf("Cantidad de goles: ");
                scanf("%u", &teams[cantTeams-1].goals);
                printf("\r\n");
                
            }else{
                printf("Solo se pueden procesar las opciones y/n\r\n");
            }
        }
    }

    for(;;){
        printf("Ingrese el orden en que quiere ver los equipos.\n");
        printf("0. Salir.\n");
        printf("1. Victorias.\n");
        printf("2. Derrotas.\n");
        printf("3. Cantidad de goles.\n\n");
        scanf("%ui", &option);

        if(option == 0){
            break;
        }

        switch (option) {
            case 1:
                sortByVictories(teams, cantTeams);
                break;
            case 2:
                sortByDefeats(teams, cantTeams);
                break;
            case 3:
                sortByGoals(teams, cantTeams);
                break;
            default:
                printf("**************************************************************\r\n");
                printf("*************** No se eligio una opcion valida ***************\r\n");
                printf("**************************************************************\r\n");
                break;
        }
    }
    


    free(teams);
    printf("***************************************************\r\n");
    printf("*************** Programa finalizado ***************\r\n");
    printf("***************************************************\r\n");
    return 0;
}