
#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("*****************************************************\r\n");
    printf("*********  Biblioteca estandar - Ejercicio 2  *******\r\n");
    printf("*****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           *******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           *******\r\n");
    printf("*****************************************************\r\n");
   
    FILE * fp;
    int count=0;
    char c;
    fp = fopen ("lorem_ipsum.txt", "r");
    while(1) {
        fread(&c, sizeof(c), 1, fp);
        if(feof(fp)) { 
            break;
        }
        if(c == ' ') {
            count++;
        }
    }
    fclose(fp);
    printf("\nSe leyeron %i palabras del archivo.\r\n\n", count+1);

    printf("*****************************************************\r\n");
    printf("**************** Programa finalizado ****************\r\n");
    printf("*****************************************************\r\n");

    return 0;
}