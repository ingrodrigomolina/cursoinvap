#include <math.h>
#include <stdio.h>

#define YEARS 30
#define INTEREST 6

double calcularInteres(int years, double interest, int dolars, int show) {
    double result=0;
    for (int i = 0; i < years; i++) {  
        result += (double) dolars * pow((1+(interest/100)), i+1);
        if(show != 0) {
            printf("Dinero acumulado año %i: USD $ %.2f \r\n", i+1 ,result);
        }  
    }
    return result;
}

int main() {
    printf("****************************************************\r\n");
    printf("****** Instrucciones de control - Ejercicio 2 ******\r\n");
    printf("****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           ******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           ******\r\n");
    printf("****************************************************\r\n");

    float dolarsByYear = 100;
    
    double total= calcularInteres(YEARS, INTEREST, dolarsByYear, 0);
    printf("*************************************************\r\n");
    printf("********** 1. Total F = USD $ %.2f ***********\r\n", total);
    printf("*************************************************\r\n\n");
    calcularInteres(YEARS, INTEREST, dolarsByYear, 1);

    float cantMoney = 100000;
    dolarsByYear = cantMoney / (total / dolarsByYear);
    printf("\n***********************************************\r\n");
    printf("********** 2. Total A = USD $ %.2f *********\r\n", dolarsByYear);
    printf("***********************************************\r\n");

    calcularInteres(YEARS, INTEREST, dolarsByYear, 1);

    printf("\n*************************************************\r\n");
    printf("**********     Programa finalizado      *********\r\n");
    printf("*************************************************\r\n");
    return 0;
}