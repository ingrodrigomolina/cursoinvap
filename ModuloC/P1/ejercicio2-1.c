#include <stdio.h>
#include <string.h>

#define LEN 80

int main() {   
    char texto[LEN];

    printf("****************************************************\r\n");
    printf("****** Instrucciones de control - Ejercicio 1 ******\r\n");
    printf("****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           ******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           ******\r\n");
    printf("****************************************************\r\n");

    for (;;) {
       printf("******  Ingrese un texto o '-1' para finalizar: ");
       scanf("%s", texto);
      
       if(strcmp(texto, "-1") == 0){
           break;
       }
       printf("******  Inverso: ");

       for (int i = strlen(texto); i > -1; i--)
       {
           printf("%c", texto[i]);
       }
       printf("\n****************************************************\r\n");
       
    }
    printf("\n****************************************************\r\n");
    printf("**********       Programa finalizado        ********\r\n");
    printf("****************************************************\r\n");

    return 0;
}