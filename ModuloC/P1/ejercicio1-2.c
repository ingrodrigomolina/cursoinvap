#include <stdio.h>

int main()
{   
    printf("*****************************************************\r\n");
    printf("****** Entrada y salida de datos - Ejercicio 2 ******\r\n");
    printf("*****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           *******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           *******\r\n");
    printf("*****************************************************\r\n");

    printf("**************        Punto 1:     ******************\r\n");

    char name[30];
    printf("******      Por favor, introduce tu nombre:    ******\r\n");
    scanf("%s", name);
    printf("\n     Bienvenido %s !!!\r\n\n", name);

    printf("**************        Punto 2:     ******************\r\n");
    float x1;
    float x2;
    printf("*******     Ingresar dos numeros decimales   ********\r\n");

    scanf("%f", &x1);
    scanf("%f", &x2);
    printf("\n X1 = %.1f  X2 = %.1f \r\n", x1, x2);

    printf("**************        Punto 3:     ******************\r\n");
    
    int value1, value2;
    printf("*** Ingresar dos numeros enteros que quiere sumar ***\r\n");
    scanf("%i", &value1);
    scanf("%i", &value2);
    printf("****************    %i + %i =  %i     ******************\r\n", value1, value2, value1 + value2);

    printf("\n*****************************************************\r\n");
    printf("**********       Programa finalizado        *********\r\n");
    printf("*****************************************************\r\n");


    return 0;
}