#include <stdio.h>
#include <math.h>

#define n           10
#define l_to_h_a    1
#define l_to_h      2
#define h_to_l_a    3
#define h_to_l      4
#define exit        0

void showOptions() {
    printf("Debe elegir el orden en que se mostrara el vector (%i, %i, %i o %i) o (%i) para salir. \n\n", l_to_h_a, l_to_h, h_to_l_a, h_to_l, exit);
    printf("%i. Ordenamiento de menor a mayor en valor absoluto. \n", l_to_h_a);
    printf("%i. Ordenamiento de menor a mayor algebraicamente (con signo). \n", l_to_h);
    printf("%i. Ordenamiento de mayor a menor en valor absoluto. \n", h_to_l_a);
    printf("%i. Ordenamiento de mayor a menor algebraicamente (con signo).\n\n", h_to_l);

}

int main() {   
    float x[n] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
    int order;
    float t;
    printf("*****************************************************\r\n");
    printf("******         Arreglos - Ejercicio 1         *******\r\n");
    printf("*****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           *******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           *******\r\n");
    printf("*****************************************************\r\n");
    showOptions();

    while (1) {
       printf("Ingrese una opcion: \n\n");
       scanf("%i", &order);
      
       if(order == exit){
            printf("*****************************************************\r\n");
            printf("**********       Programa finalizado        *********\r\n");
            printf("*****************************************************\r\n");
            break;
       }
       switch (order)
       {
        case l_to_h_a: //menor a mayor (valor absoluto)
            for (size_t i = 0; i < n-1; i++) { 
                for (size_t d = 0 ; d < n - i - 1; d++)
                {
                    if (fabs(x[d]) > fabs(x[d+1]))
                    {
                        t          = x[d];
                        x[d]   = x[d+1];
                        x[d+1] = t;
                    }
                }
            }
           break;
        case l_to_h: //menor a mayor con signo
            for (size_t i = 0; i < n-1; i++) { 
                for (size_t d = 0 ; d < n - i - 1; d++)
                {
                    if (x[d] > x[d+1])
                    {
                        t          = x[d];
                        x[d]   = x[d+1];
                        x[d+1] = t;
                    }
                }
            }
           break;
        case h_to_l_a: //mayor a menor (valor absoluto)
           for (size_t i = 0; i < n-1; i++) { 
                for (size_t d = 0 ; d < n - i - 1; d++)
                {
                    if (fabs(x[d]) < fabs(x[d+1]))
                    {
                        t          = x[d];
                        x[d]   = x[d+1];
                        x[d+1] = t;
                    }
                }
            }
           break;
        case h_to_l: //mayor a menor con signo
           for (size_t i = 0; i < n-1; i++) { 
                for (size_t d = 0 ; d < n - i - 1; d++)
                {
                    if (x[d] < x[d+1])
                    {
                        t          = x[d];
                        x[d]   = x[d+1];
                        x[d+1] = t;
                    }
                }
            }
           break;
       default:
           printf("-------La opcion de orden elegida la existe------\n\n");
           showOptions();
           break;
       }

        for (size_t i = 0; i < n; i++) {
            printf("%.1f  ", x[i]);
        } 
        printf("\n\n");
    }
   

    return 0;
}