#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define total_cards     52
#define playeds         2
#define initials_cards  2
#define card_by_type    13

int main() {   
    printf("*****************************************************\r\n");
    printf("******    Arreglos - Ejercicio 3 - BlackJack  *******\r\n");
    printf("*****************************************************\r\n");
    printf("******           █ █▄░█ █░█ ▄▀█ █▀█           *******\r\n");
    printf("******           █ █░▀█ ▀▄▀ █▀█ █▀▀           *******\r\n");
    printf("*****************************************************\r\n");

    
    time_t t;
    int used[total_cards];
    int total[playeds], card;
    int i,j;

    char types_card[4][10] = {"♣", "♦", "♥", "♠"};


    srand((unsigned) time(&t));

    //Init used array
    for ( i = 0; i < total_cards; i++) {
        used[i] = 0;
    }
    for ( i = 0; i < playeds; i++) {
        total[i] = 0;
    }
    

    
    for ( i = 0; i < playeds; i++) {
        printf("      Jugador %i         ", i+1);
    }
    printf("\n\n");
    for ( j = 0; j < initials_cards; j++)
    {
        for ( i = 0; i < playeds; i++)
        {
            
            do
            {
                card = rand()%total_cards;
            } while (used[card] == 1);
            used[card] = 1;
            total[i] += ((card%card_by_type) >8) ? 10 : (card%card_by_type)+1;
            printf("          %i %s         ", (card%card_by_type)+1, types_card[card/card_by_type]);
        }
        printf("\n\n");
    }

    printf("\n");

    for ( i = 0; i < playeds; i++)
    {   
        char next_card[1];
        printf("****************************************\n");
        printf("***********Juega jugador %i*************\n", i+1);
        printf("****************************************\n");

        while(1){
            
            printf("*******Total: %i *******\n", total[i]);
            printf("Quieres otra carta? (y/n) \n");
            scanf("%s", next_card);
            if(strcmp(next_card, "n") == 0){
                break;
            }
            if(strcmp(next_card, "y") == 0){
                  do
                {
                    card = rand()%total_cards;
                } while (used[card] == 1);
                used[card] = 1;
                total[i] += ((card%card_by_type) >8) ? 10 : (card%card_by_type)+1;
                printf("Tu carta es:  %i %s  \n", (card%card_by_type)+1, types_card[card/card_by_type]);

                if(total[i] > 21){
                    printf("****Te pasaste de 21, quedas descalificado*****\n");
                    break;
                }
            }


            printf("\n\n");
        }

        printf("\n\n");
    }

    int win = 0, max=0;
    for ( i = 0; i < playeds; i++) {
        if((total[i]<22) && (max < total[i])){
            max = total[i];
            win = i+1;
        }else{
            if(total[i] == max){
                win = 0;
            }
        }
        
    }

    if(win!= 0){
        printf("***********************************************\n");
        printf("*************** FELICITACIONES!! **************\n");
        printf("*********** Gana jugador numero %i ************\n", win);
        printf("***********************************************\n");
    }else{
        printf("***********************************************\n");
        printf("*************** Fue un empate *****************\n");
        printf("***********************************************\n");
    }

    return 0;
}